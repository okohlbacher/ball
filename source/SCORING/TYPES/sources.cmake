### the directory name ###
SET(DIRECTORY source/SCORING/TYPES)

### list all filenames of the directory here ###
SET(SOURCES_LIST
	fresnoTypes.C
)	

ADD_BALL_SOURCES("SCORING/TYPES" "${SOURCES_LIST}")
