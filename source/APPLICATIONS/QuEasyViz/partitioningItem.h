/* partitioningItem.h
 * 
 * Copyright (C) 2009 Marcel Schumann
 * 
 * This file is part of QuEasy -- A Toolbox for Automated QSAR Model
 * Construction and Validation.
 * QuEasy is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or (at
 * your option) any later version.
 * 
 * QuEasy is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PARTITIONINGITEM_H
#define PARTITIONINGITEM_H

#include <dataItem.h>
#include <inputDataItem.h>

namespace BALL
{
	namespace VIEW
	{
		class InputPartitionItem;
		
		class PartitioningItem : public DataItem
		{
			public:
				PartitioningItem(InputDataItem* input, DataItemView* miv, unsigned int folds, double& fraction, int ID=-1);
				~PartitioningItem();

				bool execute();

				enum { Type = UserType + 128 };
				int type() const { return Type; }
	
				void addToPipeline();
				void removeFromPipeline();
				
				InputDataItem* getInputItem();
				
				double getValFraction();
				unsigned int getNoFolds();
				unsigned int getID();
				
				/** Set the ID. Neccessary only after restoring a PartitioningItem from a config-file. */
				void setID(unsigned int ID);
				
				/** Adds a new external validation fold (pair of \<training-partition,test-partition\>) whose data is to be generated by this object. */
				void addFold(pair<InputPartitionItem*,InputPartitionItem*> fold);
				
				/** Removes the fold (== _both_ training- and test-partition) of the given partition  */
				void removePartition(InputPartitionItem* partition);
				
				/** checks whether all partitions have already been created */
				bool isDone();
				
				void setInput(InputDataItem* new_input);
				

			private:
				
				BALL::String getMouseOverText();
				
				InputDataItem* input_;
				
				unsigned int no_folds_;
				
				/** the fraction of compounds of the input data set that is to be used as validation set */
				double val_fraction_;
				
				/** the ID of this PartitioningItem with regard to its InputDataItem.
				It is only used during writing the config-file. */
				unsigned int id_;
				
				/** all external validation folds whose data is to be generated by this item as pairs of \<training-partition,test-partition\> */
				list<pair<InputPartitionItem*,InputPartitionItem*> > folds_;
				
				
			friend class DataItemScene;
			friend class InputDataItemIO;

		};
	}
}

#endif
